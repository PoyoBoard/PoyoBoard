<?php

$settings = array(
	"boardname" => array (
		"type" => "text",
		"default" => "PoyoBoard",
		"name" => "Board name"
	),
	"metaDescription" => array (
		"type" => "text",
		"default" => "Forum powered by PoyoBoard.",
		"name" => "Meta description"
	),
	"metaTags" => array (
		"type" => "text",
		"default" => "poyoboard",
		"name" => "Meta tags"
	),
	"dateformat" => array (
		"type" => "text",
		"default" => "d.m.y, H:i:s",
		"name" => "Date format"
	),
	"customTitleThreshold" => array (
		"type" => "integer",
		"default" => "100",
		"name" => "Custom Title Threshold"
	),
	"oldThreadThreshold" => array (
		"type" => "integer",
		"default" => "3",
		"name" => "Old Thread Threshold months"
	),
	"viewcountInterval" => array (
		"type" => "integer",
		"default" => "10000",
		"name" => "Viewcount Report Interval"
	),
	"ajax" => array (
		"type" => "boolean",
		"default" => "0",
		"name" => "Enable AJAX"
	),
	"guestLayouts" => array (
		"type" => "boolean",
		"default" => "1",
		"name" => "Show post layouts to guests"
	),
	"breadcrumbsMainName" => array (
		"type" => "text",
		"default" => "Forum index",
		"name" => "Text in breadcrumbs 'main' link",
	),
	"menuMainName" => array (
		"type" => "text",
		"default" => "Index",
		"name" => "Text in menu 'main' link",
	),
	"mailResetSender" => array (
		"type" => "text",
		"default" => "",
		"name" => "Password Reset e-mail Sender",
		"help" => "Email address used to send the pasword reset e-mails. If left blank, the password reset feature is disabled.",
	),
	"defaultTheme" => array (
		"type" => "theme",
		"default" => "default",
		"name" => "Default Board Theme",
	),
	"defaultLayout" => array (
		"type" => "layout",
		"default" => "abxd",
		"name" => "Board layout",
	),
	"showGender" => array (
		"type" => "boolean",
		"default" => "1",
		"name" => "Color usernames based on gender"
	),
	"defaultLanguage" => array (
		"type" => "language",
		"default" => "en_US",
		"name" => "Board language",
	),
	"floodProtectionInterval" => array (
		"type" => "integer",
		"default" => "30",
		"name" => "Minimum time between user posts"
	),
	"nofollow" => array (
		"type" => "boolean",
		"default" => "0",
		"name" => "Add rel=nofollow to all user-posted links"
	),
	"tagsDirection" => array (
		"type" => "options",
		"options" => array('Left' => 'Left', 'Right' => 'Right'),
		"default" => 'Left',
		"name" => "Direction of thread tags.",
	),
	"alwaysMinipic" => array (
		"type" => "boolean",
		"default" => "1",
		"name" => "Show Minipics everywhere",
	),
	"showExtraSidebar" => array (
		"type" => "boolean",
		"default" => "1",
		"name" => "Show extra info in post sidebar",
	),
	"showPoRA" => array (
		"type" => "boolean",
		"default" => "1",
		"name" => "Show Points of Required Attention",
	),
	"PoRATitle" => array (
		"type" => "text",
		"default" => "Points of Required Attention&trade;",
		"name" => "PoRA title",
	),
	"PoRAText" => array (
		"type" => "texthtml",
		"default" => "Welcome!",
		"name" => "PoRA text",
	),

	"profilePreviewText" => array (
		"type" => "textbbcode",
		"default" => "This is a sample post. You [b]probably[/b] [i]already[/i] [u]know[/u] what this is for.

[quote=Goomba][quote=Mario]Woohoo! [url=http://www.mariowiki.com/Super_Mushroom]That's what I needed![/url][/quote]Oh, nooo! *stomp*[/quote]

Well, what more could you [url=http://en.wikipedia.org]want to know[/url]? Perhaps how to do the classic infinite loop?",
		"name" => "Post Preview text"
	),

	"trashForum" => array (
		"type" => "forum",
		"default" => "1",
		"name" => "Trash forum",
	),
	"hiddenTrashForum" => array (
		"type" => "forum",
		"default" => "1",
		"name" => "Forum for deleted threads",
	),
);
?>
