# PoyoBoard
Message board software written in PHP and MySQL, Initially forked from the 3.0 release-tag build of the now-inactive 'AcmlmBoard XD' project in mid-2019, it consequently follows its original goals in providing a stable, easy-to-use AcmlmBoard-inspired software that (generally) works right out of the box.

## Notice
Remember, you are using a Git repository version if you are going to download directly or clone the repository. There may be bugs that generally won't be present in release builds of the software. There currently isn't a stable release for now, but feel free to check out what's here, especially if you're able to give us a hand with anything ^-^

## System Requirements
* PHP 7.0. Tested up to PHP 7.3.5 and working. We don't officially support 5.x series.
* MySQL 5.6 or MariaDB 10.1. Support for later versions alongside proper 'strict mode' compliance is planned.
* Web server. Apache 2.4 with libapache2-mpm-itk and non-FPM PHP will work fine.
* PHP extensions: mysqli, gd, json, xml

## Bug Reports
Please use the built-in issue tracker in this repository to report bugs. Make sure you include the following information:
* Git commit
* PHP version being used
* MySQL/MariaDB version being used
* Loaded plugins

Any other information isn't necessary except of course an actual bug description but it would be nice to know!

## Support
Currently, the only way to receive support would be to ask someone directly who is involved with the project. An official support forum may be considered in the future should this project require something like that to exist, but for now, directly asking a developer will suffice efficiently.

## Have fun!
If you're looking for some extra things for your board, go onto the main PoyoBoard GitLab group for freely-usable assets like plugins, themes and ranksets. Meanwhile, have fun with your board, even if typical message boards aren't a big hit with netizens these days. Best of luck!